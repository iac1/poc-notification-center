class Interface:
    
    def dataMessage(self, data):
        if type(data["room"]) == str and\
            type(data["socketId"]) == str and\
            type(data["message"]) == str and\
            type(data["broadcat"]) == bool:
            return True
        return False
