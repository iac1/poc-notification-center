class Utils:

    def statuscode_by_service(self, status_code:int) -> int:
        if status_code in range(500, 599):
            return 500
        # here is included status_code 300 and 400
        elif status_code in range(300, 499):
            return 400
        else:
            return 200

    def toString(self, payload:any) -> str:
        if not isinstance(payload, str):
            return str(payload)
        return payload

    def tuplaToString(self, data) -> str:
        if isinstance(data, tuple):
            return ''.join(data)
        return data
        
    def extractQueueNameFromArn(self, string) -> str:
        if "arn:aws" in string:
            return string.split(":")[::-1][0]
        return string
    
    def isDeadLetter(self, queueName:str) -> bool:
        if "dead-letter" in queueName or "dlq" in queueName:
            return True
        return False

