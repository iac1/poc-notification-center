import os, json
from service.sqs import Sqs
import controller.index as Controller
from repository.teamCommunicationProvider.index import Slack
from service.http import Http
from time import gmtime, strftime
from utils.index import Utils


SLACK_CHANNEL   = os.environ.get('SLACK_CHANNEL')
TEAMS_CHANNEL   = os.environ.get('TEAMS_CHANNEL')
utils           = Utils()
sqs             = Sqs()
http            = Http()
controller      = Controller

def handler(event, context):

    try:
        record                  = event["Records"][0]
        try:
            payload             = json.loads(record["body"])
        except Exception:
            payload             = record["body"]
            pass
        eventSourceARN          = record["eventSourceARN"]
        receiptHandle           = record["receiptHandle"]
        messageId               = record["messageId"]

        if controller.setController(payload):
            # delete queue if the process is successful
            sqs.deleteItem(eventSourceARN, receiptHandle, messageId)

    except Exception as error:

        Slack().send(SLACK_CHANNEL, json.dumps({"payload": payload, "error": repr(error), "dataTime":  strftime("%Y-%m-%d %H:%M:%S", gmtime())}), True)
        sqs.deleteItem(eventSourceARN, receiptHandle, messageId)
        # if not utils.isDeadLetter(eventSourceARN):
        #     sqs.sendItem(utils.tuplaToString(SQS_DEAD_LETTER_QUEUE), payload)
        raise ValueError(error)
    