from repository.teamCommunicationProvider.index import Teams, Slack

class TeamCommunicationController:

    def sendTeams(self, payload:object):
        return Teams().send(payload["destination"], payload["body"])
    
    def sendSlack(self, payload:object):
        return Slack().send(payload["destination"], payload["body"])
