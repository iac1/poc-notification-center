from repository.whatsappProvider.index import Whatsapp

class WhatsappController:

    def send(self, payload:object):
        return Whatsapp().send(payload["destination"], payload["body"])
