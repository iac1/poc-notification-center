from controller.smsController import SmsController
from controller.emailController import EmailController
from controller.whatsappController import WhatsappController
from controller.teamCommunicationController import TeamCommunicationController
from controller.websocketController import WebsocketController

def setController(payload:object) -> any:
    
    if payload["channel"] == "sms":
        return SmsController().send(payload)
    
    elif payload["channel"] == "email":
        return EmailController().send(payload)
    
    elif payload["channel"] == "whatsapp":
        return WhatsappController().send(payload)
    
    elif payload["channel"] == "teams":
        return TeamCommunicationController().sendTeams(payload)

    elif payload["channel"] == "slack":
        return TeamCommunicationController().sendSlack(payload)
    
    elif payload["channel"] == "websocket":
        return WebsocketController().se(payload)
    
    else:
        raise ValueError(f"The Channel [ ${payload['channel']} ] isn't available")
