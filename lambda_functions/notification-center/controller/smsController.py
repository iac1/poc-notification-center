from repository.smsProvider.index import Sms

class SmsController:

    def send(self, payload:object):
        return Sms().sendMessage(payload["destination"], payload["body"])