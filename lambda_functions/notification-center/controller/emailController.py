from repository.emailProvider.index import Email

class EmailController:

    def send(self, payload:object):
        return Email().send(payload["destination"], payload["body"]["subject"], payload["body"]["data"])
