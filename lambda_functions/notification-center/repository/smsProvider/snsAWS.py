import boto3
# from decouple import config

session = boto3.Session(
    # aws_access_key_id=config('_AWS_ACCESS_KEY_ID'),
    # aws_secret_access_key=config('_AWS_SECRET_ACCESS_KEY'),
    # region_name=config('_AWS_REGION')
)

client = session.client('sns')

class Sms:

    def sendMessage(self, PhoneNumber:str, Message:str):
        try:
            return client.publish(
                PhoneNumber=PhoneNumber,
                Message=Message
            )
        except Exception as error:
            raise ValueError(f"Unable to send message to {PhoneNumber}")
