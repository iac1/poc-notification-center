from service.http import Http

class Teams:

    def send(self, channel:str, payload:str, isCode = False):
        print("..................TEAMS.................")
        text_format = '`' if isCode else '```'
        return Http().post(
            channel, 
            {"text": f'{text_format} {payload} {text_format}'}, 
            {"Content-type": "application/json"}
        )    
