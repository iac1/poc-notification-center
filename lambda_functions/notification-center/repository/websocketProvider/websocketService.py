import socketio, time, random, os
# from interfaces.websocketProvider.websocketServiceInterface import Interface

sio             = socketio.Client()
WEBSOCKER_URL   = os.environ.get('WEBSOCKER_URL')

class Websocket:

    @sio.event
    def connect():
        print('connection established')


    @sio.event
    def disconnect():
        print('disconnected from server')


    sio.connect(WEBSOCKER_URL)

    @sio.event
    def sendMenssage(self, data):
        print('message received with ', data)
        sio.emit("ROOT", data)
        time.sleep(2)
