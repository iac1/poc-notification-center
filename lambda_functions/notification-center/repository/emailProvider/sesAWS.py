import boto3

# Create a new SES resource and specify a region.
client = boto3.client('ses')

class Email:

    def send(self, destinations:list, subject:str, data:str):

        try:
            return client.send_email(
                Destination={
                    'ToAddresses': destinations,
                },
                Message={
                    'Body': {
                        'Html': {
                            'Charset': 'UTF-8',
                            'Data': data
                        }
                    },
                    'Subject': {
                        'Charset': 'UTF-8',
                        'Data': subject
                    },
                },
                ReplyToAddresses=[],
                Source='app@infsite.org'
            )

        except Exception as error:
            raise ValueError("Unable send e-mail", error)
