import requests

class Http:

    def get(self, url, headers):
        return requests.get('GET', url, headers = headers)
    
    def post(self, url, payload, headers = {}):
        return requests.post(
            url, 
            json=payload,
            headers=headers
        )

