import json
from time import gmtime, strftime
from service.sqs import Sqs
from service.http import Http
from utils.index import Utils

request = Http()
sqs = Sqs()
utils = Utils()

class QueueDeadLetterController:

    def __init__(self, SQS_DEAD_LETTER_QUEUE:str, SLACK_CHANNEL:str) -> str:
        self.SQS_DEAD_LETTER_QUEUE = SQS_DEAD_LETTER_QUEUE,
        self.SLACK_CHANNEL = SLACK_CHANNEL
        pass

    def send(self, URL_TARGET:str, payload:object = {}, headers:object = None) -> None:
        try:

            # request to Organization
            response = request.post(URL_TARGET, payload, headers)
            # get status and validation
            if utils.statuscode_by_service(response.status_code) == 500:
                request.slack(self.SLACK_CHANNEL, json.dumps({"request": URL_TARGET, "status_code": response.status_code, "reason": response.reason, "payload": payload, "datatime":  strftime("%Y-%m-%d %H:%M:%S", gmtime())}))
                # Don't delete the message
                raise ValueError(f"UNABLE TO PROCESS DEAD-LETTER TO TARGET {URL_TARGET} - {response.reason}")

            elif utils.statuscode_by_service(response.status_code) == 400:
                if request.slack(self.SLACK_CHANNEL, json.dumps({"request": URL_TARGET, "status_code": response.status_code, "reason": response.reason, "payload": payload, "datatime":  strftime("%Y-%m-%d %H:%M:%S", gmtime())}), True):
                    # delete the message
                    return True
            else:
                # delete the message
                return True

        except Exception as error:
            raise ValueError(error)
