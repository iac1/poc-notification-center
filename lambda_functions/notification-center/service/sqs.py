import boto3
import uuid
import json
from utils.index import Utils

session = boto3.Session(
    # aws_access_key_id=config('APP_AWS_ACCESS_KEY_ID'),
    # aws_secret_access_key=config('APP_AWS_SECRET_ACCESS_KEY'),
    # region_name='us-east-2'
)
client = session.client('sqs')
utils = Utils()

class Sqs:

    def getItem(self, queueUrl:str):

        sqs_item = client.receive_message(
            QueueUrl=queueUrl
        )
        try:
            return {
                'Records': [{
                    "body": sqs_item['Messages'][0]['Body'],
                    "receiptHandle": sqs_item['Messages'][0]['ReceiptHandle'],
                    "messageId": sqs_item['Messages'][0]['MessageId'],
                    "eventSourceARN": queueUrl
                }
            ]}
        except Exception as error:
            raise ValueError(error)


    def deleteItem(self, queueUrl, receiptHandle, messageId):
        try:
            print("Deleted message: ", messageId)
            return client.delete_message(
                QueueUrl=utils.extractQueueNameFromArn(queueUrl),
                ReceiptHandle=receiptHandle
            )
        
        except Exception as error:
            print("Can't delete message", messageId)
            print(error)
            return None
        
    def sendItem(self, queueUrl:str, record:str, isFiFo = False):
        try:
            if isFiFo:
                return client.send_message(
                                QueueUrl=queueUrl,
                                MessageBody=record,
                                MessageGroupId=str(uuid.uuid1()),
                                MessageDeduplicationId=str(uuid.uuid1())
                            )
            else:
                return client.send_message(
                                QueueUrl=queueUrl,
                                MessageBody=record
                            )            
        except Exception as error:
            print(error)
            print("It can't send: ", queueUrl, isFiFo)
            return False
