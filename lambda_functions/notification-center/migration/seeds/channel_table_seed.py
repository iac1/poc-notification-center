from orator.seeds import Seeder


class ChannelTableSeed(Seeder):

    def run(self):
        """
        Run the database seeds.
        """

        channel_list = [
            {
                "channel": "whatsapp"
            },
            {
                "channel": "sms"
            },
            {
                "channel": "email"
            },
            {
                "channel": "websocket"
            }
        ]
        
        for item in channel_list:
            for key, value in item.items():
                self.db.table('channel').insert({key: value})


