from orator.seeds import Seeder
from seeds.nature_table_seed import NatureTableSeed
from seeds.channel_table_seed import ChannelTableSeed

class DatabaseSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.call(NatureTableSeed)
        self.call(ChannelTableSeed)


