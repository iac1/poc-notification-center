from orator.seeds import Seeder


class NatureTableSeed(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        natures = [
            {
                "nature": "charges",
                "hits": 1
            },
            {
                "nature": "promotions",
                "hits": 2
            },
            {
                "nature": "transactions",
                "hits": -1
            }
        ]
        
        for item in natures:
            self.db.table('nature').insert(
                {
                    "nature": item['nature'],
                    "hits": item['hits']
                }
            )

