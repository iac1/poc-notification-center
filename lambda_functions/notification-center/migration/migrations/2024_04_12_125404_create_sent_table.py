from orator.migrations import Migration


class CreateSentTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.db.transaction():
            with self.schema.create('sent') as table:
                table.increments('id')
                table.integer('nature_id').unsigned()
                table.foreign('nature_id').references('id').on('nature')
                table.string('destination')
                table.integer('channel_id').unsigned()
                table.foreign('channel_id').references('id').on('channel')
                table.timestamps()
                table.soft_deletes()

    def down(self):
        """
        Revert the migrations.
        """
        with self.db.transaction():
            self.schema.drop('sent')
