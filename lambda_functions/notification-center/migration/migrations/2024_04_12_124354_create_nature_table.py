from orator.migrations import Migration


class CreateNatureTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('nature') as table:
            table.increments('id')
            table.string('nature')
            table.integer('hits')
            table.timestamps()
            table.soft_deletes()
            pass

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('nature')
