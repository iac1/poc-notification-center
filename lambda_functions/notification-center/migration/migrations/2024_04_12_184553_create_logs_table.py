from orator.migrations import Migration


class CreateLogsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('logs') as table:
            table.increments('id')
            table.string('destination')
            table.string('channel')
            table.string('nature')
            table.timestamps()
            table.soft_deletes()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('logs')
