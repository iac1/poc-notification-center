from orator.migrations import Migration


class CreateChennelTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('channel') as table:
            table.increments('id')
            table.string('channel')
            table.timestamps()
            table.soft_deletes()
            pass

    def down(self):
        """
        Revert the migrations.
        """        
        self.schema.drop('channel')
