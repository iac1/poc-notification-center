-- `notification-center`.quota_view source

CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `notification-center`.`quota_view` AS
SELECT 
COUNT(`sent`.`destination`) AS `total_hits_destination`,
`sent`.`destination`,
`nature`.hits as `max_hits_nature`,
`nature`.`nature` as `nature`,
`channel`.`channel` as `channel`,
CASE
    WHEN COUNT(`sent`.`destination`) >= `nature`.`hits` AND `nature`.`hits` != -1 THEN 'exceeded' 
    ELSE 'free' 
    END AS stage
	    
FROM  `notification-center`.`sent` sent
join `notification-center`.`nature` on (`nature`.`id` = `sent`.`nature_id`) 
join `notification-center`.`channel` on (`channel`.`id` = `sent`.`channel_id`) 
GROUP BY `sent`.`destination`, `nature`.`id`, `channel`
ORDER BY `total_hits_destination`  DESC;