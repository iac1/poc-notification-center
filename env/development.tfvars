profile        = "infsite"
region         = "us-east-2"
aws_account_id = "010064566074"
function_name  = "notification-center"

# Queus
queues = [{
  name                       = "tf-development-notification-center"
  visibility_timeout_seconds = 30
  },
  {
    name                       = "tf-development-notification-center-dead-letter.fifo"
    visibility_timeout_seconds = 35
}]

# Lambda environment
function_environment = {
  SQS_DEAD_LETTER_QUEUE      = "https://sqs.us-east-1.amazonaws.com/010064566074/tf-development-notification-center-dead-letter.fifo"
  SLACK_CHANNEL              = "https://hooks.slack.com/services/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"
  TEAMS_CHANNEL              = "https://infosystechnologies.webhook.office.com/webhookb2/159fc07f-f6a3-4fd3-9f72-b7ec25f102ef@63ce7d59-2f3e-42cd-a8cc-be764cff5eb6/IncomingWebhook/d3786fd635a3421f9ed5f76f0755a756/8df134e8-9c7e-4367-9af9-3122ad6532fb"
  WHATSAPP_BROKER_URL        = "https://api.botmaker.com/v2.0"
  WEBSOCKER_URL              = "http://localhost:9000"
}

# SES
ses_emails_identity = ["marceloviana@infsite.org", "app@infsite.org"]
ses_templates        = [{
                        template_name    = "boasvindas"
                        subject = "Greetings, {{name}}!"
                        html    = "<h1>Hello {{name}},</h1><p>Your favorite animal is {{favoriteanimal}}.</p>"
                      }]

