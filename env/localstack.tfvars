profile        = "localstack"
region         = "us-east-1"
aws_account_id = "000000000000"
function_name  = "notification-center"

queues = [{
  name                       = "tf-development-notification-center"
  visibility_timeout_seconds = 30
  },
  {
    name                       = "tf-development-notification-center-dead-letter.fifo"
    visibility_timeout_seconds = 35
}]

function_environment = {
  SQS_DEAD_LETTER_QUEUE      = "https://sqs.us-east-1.amazonaws.com/000000000000/tf-development-notification-center-dead-letter.fifo"
  SLACK_CHANNEL              = "https://hooks.slack.com/services/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"
  TEAMS_CHANNEL              = "https://infosystechnologies.webhook.office.com/webhookb2/159fc07f-f6a3-4fd3-9f72-b7ec25f102ef@63ce7d59-2f3e-42cd-a8cc-be764cff5eb6/IncomingWebhook/d3786fd635a3421f9ed5f76f0755a756/8df134e8-9c7e-4367-9af9-3122ad6532fb"
  WHATSAPP_BROKER_URL        = "https://api.botmaker.com/v2.0"
  WEBSOCKER_URL              = "https://apigateway...."
}