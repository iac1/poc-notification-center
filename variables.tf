variable "function_name" {
  type        = string
  description = "Resources name."
}

variable "profile" {
  type = string
}

variable "region" {
  type = string
}

variable "aws_account_id" {
  type = string
}

variable "visibility_timeout_seconds" {
  type    = number
  default = 30
}
variable "queues" {
  type = list(any)
}

variable "function_environment" {
  type = object({
    SQS_DEAD_LETTER_QUEUE = string
    SLACK_CHANNEL         = string
  })
}

variable "ses_emails_identity" {
  type = list(any)
}

variable "ses_templates" {
  type = list(object({
    template_name = string
    subject = string
    html = string
  }))
}