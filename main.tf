provider "aws" {
  region  = var.region
  profile = var.profile
}

terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.21.0"
    }
  }
  
}

##
# notification-center
##
module "lambda_notification_center" {

  source                = "./modules/lambda"
  aws_account_id        = var.aws_account_id
  function_name         = "tf-${terraform.workspace}-${var.function_name}"
  handler               = "index.handler"
  output_path           = "${path.module}/.output/${var.function_name}.zip"
  output_path_layer     = "${path.module}/lambda_layers/${var.function_name}.zip"
  function_dir          = "${path.module}/lambda_functions/${var.function_name}/"
  json_policy           = file("${path.module}/policies/${var.function_name}.json")
  environment_variables = var.function_environment
  region                = var.region
  runtime               = "python3.12"
  timeout               = 10

}

module "trigger_sqs" {

  count                      = length(var.queues)
  source                     = "./modules/sqs_trigger_lambda"
  function_name              = module.lambda_notification_center.function_name
  queue_name                 = var.queues[count.index].name
  visibility_timeout_seconds = var.queues[count.index].visibility_timeout_seconds
  fifo_queue                 = (length(regexall(".*.fifo", var.queues[count.index].name)) > 0) ? true : false
}

module "ses_email_identity" {
  count     =  length(var.ses_emails_identity)
  source    = "./modules/ses/sesEmailIdentity"
  email     = var.ses_emails_identity[count.index]
}

module "ses_template" {
  count     =  length(var.ses_templates)
  source    = "./modules/ses/sesTemplate"
  template_name = var.ses_templates[count.index].template_name
  subject = var.ses_templates[count.index].subject
  html = var.ses_templates[count.index].html
}