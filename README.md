<center>
<H1> IaC Terraform for POC-NOTIFICATION-CENTER with Python application in Lambda and with SQS Message Broker  </H1>
</center>

![See here archtecture diagram](./diagram/Notification.drawio.svg)

### Setup for development

1. Intall and configure the AWS and Terraform CLI.

2. Create a IAM user with programmatic access to AWS for Terraform to use.


### Prepare environment

- Add the correct environments inside a file named:
```
   # environment name (development, staging or production)
    /env/development.tfvars
 ```

- Create of Workspace to environments:
```
    # environment name (development, staging or production)
    terraform workspace new development
```

### Start Terraform
 ```
    terraform init
```

### Destroy to AWS

- Destroy:
 ```
    terraform workspace select development && terraform destroy -var-file=env/development.tfvars -target=module.NAME1 -target=module.NAME2... 
 ```

### Plan

- See the plan:
 ```
    terraform workspace select development && terraform plan -var-file=env/development.tfvars
 ```

### Deploy to AWS
- Apply the infrastructure:

```
    terraform workspace select development && terraform apply -var-file=env/development.tfvars -parallelism=3
```
### Python version and Packages

Version: 3.12.2
Package:
- pip install orator
- brew install mysql pkg-config
- pip install "cython<3.0.0" wheel
- pip install "pyyaml==5.4.1" --no-build-isolation
- pip install PyMySQL
- pip install mysqlclient
- pip install python-socketio
<!-- - pip install pydantic -->

### Terraform modules for this project:
 - SES
 - SNS
 - SQS
 - Lambda
 - CloudWatch Group Logs


<center>
<H2> POC application for Notification-Center </H2>
</center>


PATH: 
` ./lambda_functions/notification-center/ `


#### Structure directory

```

└── notification-center
    ├── controller
    ├── migration
    │   ├── migrations
    │   ├── orator.yml
    │   ├── seeds
    │   └── views
    ├── repository
    │   ├── emailProvider
    │   ├── smsProvider
    │   └── whatsappProvider
    ├── service
    └── utils

```

### Current channels:
    - sms
    - email
    - whatsApp
    - teams
    - slack
    - websocket

### Current natural:
    - promotions
    - charges
    - transactions


### Payload example for SMS or WhatsApp
```
    # SMS
    {
        "natural": string,
        "destination": string,
        "channel": "sms",
        "headers": {},
        "body": string
    } 

    # WhatsApp    
    {
        "natural": string,
        "destination": string,
        "channel": "whatsapp",
        "headers": {},
        "body": string
    }     
```

### Payload example for E-mail
```
    {
        "natural": string,
        "destination": array,
        "channel": "email",
        "headers": {},
        "body": {
                "subject": string,
                "data": HTML
        }
    }    
```

### Payload example for Websocket
```
    {
        "natural": string,
        "destination": string,
        "channel": string,
        "headers": {},
        "body": {
                    "room": "ROOT",
                    "socketId": "2voX8V6NRcR97-EzAAAr",
                    "message": f"from backend (1) - {sio.sid}",
                    "broadcast": False
                }
    }

   
```

### Payload example for Teams and Slack


```
    # Slack
    {
        "natural": string,
        "destination": https://slack-incoming-webhook,
        "channel": "slack",
        "headers": {},
        "body": {}
    }
    # Teams
    {
        "natural": string,
        "destination": "https://teams-incoming-webhook",
        "channel": "teams",
        "headers": {},
        "body": {}
    } 
```


### How to use on client:


In the Client we can use the npm package as below:

```
    npm install notification-center-client # no build
```
This package contains the contract and data structure between client and server (notification-center).



# Orator ORM and Migration



### Required packages
```
pip install orator
brew install mysql pkg-config
pip install "cython<3.0.0" wheel
pip install "pyyaml==5.4.1" --no-build-isolation
pip install PyMySQL
pip install mysqlclient
# websocket:
pip install python-socketio


```

#### Architect and developer: [Marcelo Viana](https://www.linkedin.com/in/marcelovianaalmeida/)