# Lambda function
resource "aws_lambda_function" "lambda_main" {
  filename         = var.output_path
  function_name    = var.function_name
  role             = aws_iam_role.lambda_role.arn
  handler          = var.handler
  source_code_hash = data.archive_file.source_code_file.output_base64sha256
  runtime          = var.runtime
  timeout          = var.timeout
  layers           = [aws_lambda_layer_version.lambda_layer.arn]

  environment {
    variables = var.environment_variables
  }

  depends_on = [
    aws_cloudwatch_log_group.lambda_main,
    data.archive_file.source_code_file
  ]
}

resource "aws_cloudwatch_log_group" "lambda_main" {
  name              = "/aws/lambda/${var.function_name}"
  retention_in_days = 1
}

# adds policy to role
resource "aws_iam_role_policy" "lambda_main" {
  name   = aws_lambda_function.lambda_main.function_name
  role   = aws_iam_role.lambda_role.name
  policy = var.json_policy
}

# lambda role
resource "aws_iam_role" "lambda_role" {
  name = "${var.function_name}-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
        Effect = "Allow",
        Sid    = ""
      }
    ]
  })
}

# source code to lambda deploy
data "archive_file" "source_code_file" {
  type        = "zip"
  source_dir  = var.function_dir
  output_path = var.output_path
}

resource "aws_lambda_layer_version" "lambda_layer" {
  filename   = "./lambda_layers/requests.zip"
  layer_name = "requests"

  compatible_runtimes = ["${var.runtime}"]
}
