output "lambda_arn" {
  value = aws_lambda_function.lambda_main.arn
}

output "function_name" {
  value = aws_lambda_function.lambda_main.function_name
}